import React from 'react';

const ITEMS = [
    { title: 'Pen', link: 'http://dummyimage.com/600x400', description: "This is simply a pen"},
    { title: 'Mug', link: 'http://dummyimage.com/600x400', description: "This is simply a mug"},
    { title: 'Ruler', link: 'http://dummyimage.com/600x400', description: "This is simply a ruler"},
];

const ItemDetail = (props) => {
    //props.item => this is the item object
    return (
        <div>
            <li className="media list-group-item">
                <div className="item-image">
                    <img src={props.item.link} />
                </div>
                <div className="media-body">
                    <h4 className="item-heading">
                        {props.item.title}
                    </h4>
                </div>
                <div className="item-description">
                    <p className="item-description-text">
                        {props.item.description}
                    </p>

                </div>
            </li>

        </div>
    );
};


const ItemsContainer = () => {
    const RenderedItems = ITEMS.map(function(item){
        return (
            <ItemDetail key={item.title} item={item}/>
        )
    });

    return (
        <ul className="media-list list-group">
            {RenderedItems}
        </ul>
    );
};



export default ItemsContainer;
