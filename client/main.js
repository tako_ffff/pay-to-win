import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import ReactDOM from 'react-dom';

import ItemsContainer from './source/items';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = { images : []};
    }
    conponentWillMount () {     // this place is for loading data before render,
                                // for detail see "load data with life cycle method"
        console.log('App is about to render');  //replace this line when database is ready
//      axios.get('http://some/api/here/').then(response => this.setState({ images: response.data.data})); //haven't install axios yet
        // NEVER DO THIS-
        // this.state.images = [{},{}];
    }
    render() {
        console.log(this.state.images); //sample on passing in states from parent component
        return (
            <div>
                <h1>Items List</h1>
                <ItemsContainer />
            </div>
        );
    }
};

Meteor.startup(() => {
  ReactDOM.render(<App />, document.querySelector('.container'));
});
